import {Component} from "react";
import ProductList from "./ProductList";

class App extends Component{

state =
    {
        data: []
    }

    componentDidMount() {
    fetch('http://localhost:8080/products/all')
        .then(response => response.json())
        .then(data => { this.setState({ data }) });
    }


    render() {
        return (
        <div>
            <span>---------------</span>
            {this.state.data.map(product => <ProductList key={product.id} info={product}/>)}
            
        </div>
    );
    }


}

export default App;
